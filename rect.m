%% 
clc; clear;
% load('stereo_cal_irir.mat')
load('irir_params.mat')
ir1=imread('20210706_150249_ir2_rescale/ir2_0084.tif');
% vis=imread('20210706_150249_vis_cal/vis_0084.tif');

vis=imread('20210706_150249_ir1_rescale/ir1_0084.tif');
vis=cat(3, vis, vis, vis);

ir1=cat(3, ir1, ir1, ir1);
% ir1=mat2gray(ir1);
% vis=mat2gray(vis);

% figure;
% imshow(ir1)

% figure;
% imshow(vis)

[recl, recr] = rectifyStereoImages(vis, ir1, stereoParams, 'OutputView','valid');

% figure;
% imshow(recl)

% figure;
% imshow(recr)

% figure; 
% imshow(stereoAnaglyph(recl,recr));
% imtool(stereoAnaglyph(recl,recr));

J1 = rgb2gray(recl);
J2 = rgb2gray(recr);

% figure;
% imhist(J1)
% J1b = double(J1)./double(max(J1-min(J1)))
% imhist(J1b)
% 
% figure;
% J1n = floor(255*mat2gray(J1));
% imshow(J1n)
% figure;
% imhist(J1n)
% figure;
% J2n = floor(255*mat2gray(J2));
% imshow(J2n)

disparityRange = [0 48];
% disparityMap = disparitySGM(J1,J2,'DisparityRange',disparityRange,'UniquenessThreshold',20);
disparityMap = disparityBM(J1,J2,'DisparityRange',disparityRange,'UniquenessThreshold',20, 'BlockSize', 7);

figure
% imtool(disparityMap)
imshow(disparityMap,disparityRange)
title('Disparity Map')
colormap jet
colorbar