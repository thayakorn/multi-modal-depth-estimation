%%
input_dir = './20210831_193318';
% addpath(genpath('fusion'))
% addpath('RI');

%% Rescale IR and demosaic Vis
method_demosaic = 0; % 0: MATLAB, 1: RI
rescale_demosaic(input_dir, method_demosaic);

%% Make image for calibration
start_num = 1;
skip_num = 1;
make_cal_image(input_dir, start_num, skip_num);
