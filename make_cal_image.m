function make_cal_image(input_dir, start_num, skip_num)
%%
if input_dir(end) == '/' || input_dir(end) == '\'
    input_dir = input_dir(1:end-1);
end

input_path_ir1 = strcat(input_dir, '_ir1_rescale/');
input_path_ir2 = strcat(input_dir, '_ir2_rescale/');
input_path_vis = strcat(input_dir, '_vis_demsc/');
output_path_ir1 = strcat(input_dir, '_ir1_cal/');
output_path_ir2 = strcat(input_dir, '_ir2_cal/');
output_path_vis = strcat(input_dir, '_vis_cal/');

fileList_ir1 = dir( strcat(input_path_ir1, 'ir1_*') );
fileList_ir2 = dir( strcat(input_path_ir2, 'ir2_*') );
fileList_vis = dir(strcat(input_path_vis, 'vis_*'));
numfiles = length(fileList_vis);

%%
mkdir(output_path_ir1)
mkdir(output_path_ir2)
mkdir(output_path_vis)

%%
for i = start_num:skip_num:numfiles
    fname_ir1 = fileList_ir1(i).name;
    fname_ir2 = fileList_ir2(i).name;
    fname_vis = fileList_vis(i).name;
    
    ir1 = imread(strcat(input_path_ir1, fname_ir1));
    ir2 = imread(strcat(input_path_ir2, fname_ir2));
    vis = imread(strcat(input_path_vis, fname_vis));
    
    ir1 = imcomplement(ir1);
    ir2 = imcomplement(ir2);
%     ir1 = imresize(ir1, 3, 'box');
%     ir2 = imresize(ir2, 3, 'box');
    
    vis = uint8(vis ./ 2^8);
    vis = vis(:, 65:1984, :);
    vis = imresize(vis, 1/3, 'box');
    
    imwrite(ir1, strcat(output_path_ir1, fname_ir1));
    imwrite(ir2, strcat(output_path_ir2, fname_ir2));
    imwrite(vis, strcat(output_path_vis, fname_vis));
end
end
