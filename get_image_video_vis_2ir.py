import os  # os.getcwd()
import time
from pathlib import Path

import numpy as np  # pip install numpy
from PIL import Image as PIL_Image  # pip install Pillow

from arena_api.system import system

import ctypes

import cv2
import time
import datetime
import sys
import numpy as np


from WDT import *

def create_devices_with_tries():
    tries = 0
    tries_max = 6
    sleep_time_secs = 10
    while tries < tries_max:  # Wait for device for 60 seconds
        devices = system.create_device()
        if not devices:
            print(
                f'Try {tries+1} of {tries_max}: waiting for {sleep_time_secs} '
                f'secs for a device to be connected!')
            for sec_count in range(sleep_time_secs):
                time.sleep(1)
                print(f'{sec_count + 1 } seconds passed ',
                      '.' * sec_count, end='\r')
            tries += 1
        else:
            print(f'Created {len(devices)} device(s)')
            return devices
    else:
        raise Exception(f'No device found! Please connect a device and run '
                        f'the example again.')

def make_vis_array(vis_frame_buffer):
    pdata16 = ctypes.cast(vis_frame_buffer.pdata,
                            ctypes.POINTER(ctypes.c_ushort))
    vis_nparray_reshaped = np.ctypeslib.as_array(
       pdata16,
      (vis_frame_buffer.height, vis_frame_buffer.width))
    vis_array = PIL_Image.fromarray(vis_nparray_reshaped)
    return vis_array

def make_view_image(ir_frame):
    ir_view = ir_frame
    ir_max = ir_view.max()
    im_min = ir_view.min()
    ir_view = np.array( (ir_view - im_min) / (ir_max - im_min) * 255.0,
                dtype = 'uint8')
    return ir_view

def example_entry_point(ir_device_index1, ir_device_index2, cap_time):
    # Create devices
    vis_devices = create_devices_with_tries()
    vis_device = vis_devices[0]
    print(f'Device used (Visible):\n\t{vis_device}')

    ir_device_index1 = 1
    ir_cap1 = cv2.VideoCapture(ir_device_index1+cv2.CAP_DSHOW)
    ir_cap1.set(cv2.CAP_PROP_FOURCC, cv2.VideoWriter.fourcc('Y','1','6',' '))
    ir_cap1.set(cv2.CAP_PROP_CONVERT_RGB, False)

    ir_device_index2 = 2
    ir_cap2 = cv2.VideoCapture(ir_device_index2+cv2.CAP_DSHOW)
    ir_cap2.set(cv2.CAP_PROP_FOURCC, cv2.VideoWriter.fourcc('Y','1','6',' '))
    ir_cap2.set(cv2.CAP_PROP_CONVERT_RGB, False)

    # Get/Set nodes
    nodes = vis_device.nodemap.get_node(['Width', 'Height', 'PixelFormat'])

    print('Setting Width to its maximum value')
    nodes['Width'].value = nodes['Width'].max

    print('Setting Height to its maximum value')
    height = nodes['Height']
    height.value = height.max

    pixel_format_name = 'BayerRG16'
    print(f'Setting Pixel Format to {pixel_format_name}')
    nodes['PixelFormat'].value = pixel_format_name

    # Set outputs
    filetime = datetime.datetime.now()
    output_path = filetime.strftime("%Y%m%d_%H%M%S")
    output_path += '/'

    try:
        os.makedirs(output_path)
    except FileExistsError:
        pass

    # Make window
    cv2.namedWindow("image1")
    cv2.namedWindow("image2")

    with vis_device.start_stream(1):

        print('Starting stream')
        vis_frame_buffer = vis_device.get_buffer()
        code1, ir_frame1 = ir_cap1.read()
        code2, ir_frame2 = ir_cap2.read()
        cv2.waitKey(100)
        vis_device.requeue_buffer(vis_frame_buffer)

        # Set time to stop
        if cap_time > 0:
            td = datetime.timedelta(seconds=cap_time)

        # Set inital
        img_n = 0
        timestamps = []
        time_start = datetime.datetime.now()

        sfp = SleepForPeriodic(0.5)
        while True:
            sfp.start()

            # Get time
            time_now = datetime.datetime.now()
            timestamps.append(time_now)
            
            # Grab and save an image buffer
            vis_frame_buffer = vis_device.get_buffer()
            code1, ir_frame1 = ir_cap1.read()
            code2, ir_frame2 = ir_cap2.read()

            # Write image to file
            vis_name = f'{output_path + "vis_" + str(img_n).zfill(4)}.tif'
            vis_array = make_vis_array(vis_frame_buffer)
            vis_array.save(vis_name)
            vis_device.requeue_buffer(vis_frame_buffer)

            ir_name1 = f'{output_path + "ir1_" + str(img_n).zfill(4)}.tif'
            cv2.imwrite(ir_name1, ir_frame1)
            ir_name2 = f'{output_path + "ir2_" + str(img_n).zfill(4)}.tif'
            cv2.imwrite(ir_name2, ir_frame2)

            # Show image
            cv2.imshow('image1', make_view_image(ir_frame1))
            cv2.imshow('image2', make_view_image(ir_frame2))

            # Stop if q is pressed
            if cv2.waitKey(1) & 0xFF == ord('q'):
                break

            # Show number of images taken
            print("\rImage number: " + str(img_n), end="")
            sys.stdout.flush()

            # Stop if cap_time
            if cap_time > 0:
                if time_now - time_start > td:
                    break

            img_n += 1
            sfp.sleep()

    print('\nEnd stream')

    # Clean up
    system.destroy_device()
    cv2.destroyAllWindows()
    ir_cap1.release()
    ir_cap2.release()
    print('Destroyed all created devices\n')

    # Output timestamps
    time_name = f'{output_path + "time.txt"}'
    with open(time_name, mode='w') as f:
        for timedata in timestamps:
            t = timedata.strftime("%Y/%m/%d %H:%M:%S.%f")
            print(t)
            t += '\n'
            f.write(t)

if __name__ == '__main__':
    ir_device_index1 = 1
    ir_device_index2 = 2

    args = sys.argv
    if len(args) != 2:
        print('Usage: python get_image_video_vis_2ir.py cap_time')
    else:
        cap_time = int(sys.argv[1])
        print('\nWARNING:\nTHIS EXAMPLE MIGHT CHANGE THE DEVICE(S) SETTINGS!')
        print('\nExample started\n')
        example_entry_point(ir_device_index1, ir_device_index2, cap_time)
        print('\nExample finished successfully')
