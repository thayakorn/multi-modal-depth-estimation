%% save rectified images

clc; clear;
% load('stereo_cal_irir.mat')
load('20210831_193628_ir1_vis.mat')

%%
input_dir = './';
input_path_ir1 = strcat(input_dir, '20210831_193318_ir1_rescale')
input_path_vis = strcat(input_dir, '20210831_193318_vis_cal')
output_path_ir1 = strcat(input_dir, '20210831_193318_ir1_rectified')
output_path_vis = strcat(input_dir, '20210831_193318_vis_rectified')

fileList_ir1 = dir( strcat(input_path_ir1, '/ir1_*') );
numfiles_ir1 = length(fileList_ir1);
fileList_vis = dir( strcat(input_path_vis, '/vis_*') );
numfiles_vis = length(fileList_vis);
%% 

mkdir(output_path_ir1)
mkdir(output_path_vis)
%% 
for i = 1:numfiles_ir1
    fname_ir1 = fileList_ir1(i).name;
    fname_vis = fileList_vis(i).name;
    ir1=imread(strcat(input_path_ir1, '/', fname_ir1));
    vis=imread(strcat(input_path_vis, '/', fname_vis));
    ir1=cat(3, ir1, ir1, ir1);
    
    
    [rec_l, rec_r] = rectifyStereoImages(ir1, vis, stereoParams, 'OutputView','valid');
    imwrite(rec_l, strcat(output_path_ir1, '/', fname_ir1));
    imwrite(rec_r, strcat(output_path_vis, '/', fname_vis));
end

