# Multi Modal Depth Estimation

boson_16bit.py	遠赤外線カメラのデバイス番号確認用
14行目のdevice_indexがデバイス番号(おそらく1と2)

# Usage
Shooting with a visible camera and 2 IR cameras
```
python get_image_video_vis_2ir.py cap_time
```

### Parameters
cap_time：capture time(sec) If 0, shoot until 'q' is pressed on the keyboard.