%% 
load('20210831_193628_ir1_vis.mat')

R = stereoParams.RotationOfCamera2;
t = stereoParams.TranslationOfCamera2;
Pl = [stereoParams.CameraParameters1.IntrinsicMatrix' [0; 0; 0]]*[ R t'; 0 0 0 1]
Mr = [stereoParams.CameraParameters2.IntrinsicMatrix' [0; 0; 0]]

ul = [248.0; 235.0]
ur = [220.0; 221.0]

A = [ur(1)*Mr(3,1)-Mr(1,1) ur(1)*Mr(3,2)-Mr(1,2) ur(1)*Mr(3,3)-Mr(1,3);
    ur(2)*Mr(3,1)-Mr(2,1) ur(2)*Mr(3,2)-Mr(2,2) ur(2)*Mr(3,3)-Mr(2,3);
    ul(1)*Pl(3,1)-Pl(1,1) ul(1)*Pl(3,2)-Pl(1,2) ul(1)*Pl(3,3)-Pl(1,3);
    ul(2)*Pl(3,1)-Pl(2,1) ul(2)*Pl(3,2)-Pl(2,2) ul(2)*Pl(3,3)-Pl(2,2)];

b = [Mr(1,4)-Mr(3,4); Mr(2,4)-Mr(3,4); Pl(1,4)-Pl(3,4); Pl(2,4)-Pl(3,4)];

inv((A'*A))*A'*b
%% 
t = stereoParams.TranslationOfCamera2;
b = abs(t(1))*
