from __future__ import division

import os

import colour
from colour.plotting import *

from colour_demosaicing import (
    EXAMPLES_RESOURCES_DIRECTORY,
    demosaicing_CFA_Bayer_bilinear,
    demosaicing_CFA_Bayer_Malvar2004,
    demosaicing_CFA_Bayer_Menon2007,
    mosaicing_CFA_Bayer)

cctf_encoding = colour.cctf_encoding

colour.utilities.filter_warnings()

colour.utilities.describe_environment()

input_image_name = "steam3/vis_0000.tif"
input_image = colour.io.read_image(input_image_name)

#output_image = cctf_encoding(demosaicing_CFA_Bayer_bilinear(input_image))
#output_image = cctf_encoding(demosaicing_CFA_Bayer_Malvar2004(input_image))
output_image = cctf_encoding(demosaicing_CFA_Bayer_Menon2007(input_image))
plot_image(output_image)
